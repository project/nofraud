CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

 ![NoFraud](https://portal.nofraud.com/images/nfgreen.png)

 The NoFraud API implementation is used to validate all payment transactions.

 [Here](https://portal.nofraud.com/pages/developer-documentation) is the
 developer documentation for more information about the API.

 The module consists of a service that connects to the NoFraud API and has the
 possibility to validate a payment transaction by giving the bin number, the
 payment amount number, and the last 4 digits of the user's credit card.

 The module has a dedicated webhook for allowing NoFraud service to do requests
 to Drupal system in order to update a trasaction record status. In order to do
 that, NoFraud needs to provide such a payload example for this endpoint:

 Endpoint: `/webhook/nofraud/update-status`
 Payload: decision (string), decision_id (string)
 Headers: NO-FRAUD-API-KEY (string)

 Exampple request:
 Payload:
 {
  decision: "failed",
  decision_id: "521c1268-54d5-545f-90f8-b2d0e7a34ff2"
 }
 Headers: NO-FRAUD-API-KEY: NF_prod_12345678abc1e2de001c147858d12323ea

 Example response 200:
 {
    message: "The transaction with the c7dbbd1c-5e97-5dcc-bc8f-6f9d523f41ba
    was updated to fail with success!"
 }

 Example response 400:
 {
   message: "Bad request. Missing the api key."
 }

REQUIREMENTS
-------------------

 * commerce:commerce_payment

INSTALLATION
------------

 * Install as usual, see
   https://www.drupal.org/docs/extending-drupal/installing-modules for further
   information.

CONFIGURATION
-------------

 * Here is the NoFraud API configuration: `/admin/commerce/config/nofraud`.

TROUBLESHOOTING
---------------

 * For error handling, check the `/admin/reports/dblog`, channel `nofraud`.


MAINTAINERS
-----------

Current maintainers:

* [Vesterli Andrei](https://www.drupal.org/u/andreivesterli)
* [Miloš Denčev (thenchev)](https://www.drupal.org/u/thenchev)
* [Dylan Fontaine (dylf)](https://www.drupal.org/u/dylf)
