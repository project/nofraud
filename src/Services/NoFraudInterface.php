<?php

declare(strict_types=1);

namespace Drupal\nofraud\Services;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * An interface for the NoFraud service.
 */
interface NoFraudInterface {

  /**
   * Get the nofraud transaction record by the given order entity.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order object.
   */
  public function getNoFraudTransactionByOrder(OrderInterface $order);

  /**
   * Cancel the NoFraud transaction record on both Drupal/NoFraud sides.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order object.
   */
  public function cancelTransaction(OrderInterface $order);

  /**
   * Perform a request to the NoFraud API with the order transaction data.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order object.
   */
  public function sendTransaction(OrderInterface $order);

  /**
   * Get the list of the possible NoFraud transaction decision statuses.
   *
   * @return array
   *   The list of the possible decision statuses as an associative array.
   */
  public static function getTransactionStatuses(): array;

}
