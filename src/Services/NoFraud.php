<?php

declare(strict_types=1);

namespace Drupal\nofraud\Services;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\profile\Entity\ProfileInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\RequestOptions;

/**
 * Service for implementation of the NoFraud service.
 */
class NoFraud implements NoFraudInterface {

  use StringTranslationTrait;
  use LoggerChannelTrait;

  const BASE_URL = 'https://api.nofraud.com';
  const BASE_URL_SANDBOX = 'https://apitest.nofraud.com';
  const CANCELLATION_URL = 'https://portal-api.nofraud.com/api/v1/transaction-update/cancel-transaction';

  /**
   * Guzzle\Client instance.
   *
   * @var \Guzzle\Client
   */
  protected Client $httpClient;

  /**
   * Config factory manager service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected ConfigFactory $configFactory;

  /**
   * The NoFraud API url.
   *
   * @var string
   */
  protected string $apiUrl;

  /**
   * The NoFraud API Key.
   *
   * @var string
   */
  protected string $apiKey;

  /**
   * The NoFraud debug mode.
   *
   * @var string
   */
  protected string $debug;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The NoFraud service constructor.
   *
   * @param GuzzleHttp\Client $http
   *   HTTP client.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Config factory manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(
    Client $http,
    ConfigFactory $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler,
  ) {
    $this->httpClient = $http;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;

    $config = $this->configFactory->get('nofraud.settings');

    // Se the API Key.
    $this->apiKey = $config->get('apikey') ? $config->get('apikey') : '';
    // Set the base API url.
    $this->apiUrl = self::BASE_URL;
    if ($config->get('mode') == 'sandbox') {
      $this->apiUrl = self::BASE_URL_SANDBOX;
    }
    $this->apiUrl = $this->apiUrl;
    $this->debug = $config->get('debug');
  }

  /**
   * Parse the profile object data.
   *
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   The profile entity.
   *
   * @return array
   *   Return the profile organized data (if available) or an empty array.
   */
  protected function parseProfileData(?ProfileInterface $profile): array {
    if (!$profile || !$profile->address->first()) {
      return [];
    }
    $address = $profile->address->first();
    return [
      'firstName' => $address->getGivenName(),
      'lastName' => $address->getFamilyName(),
      'address' => $address->getAddressLine1() . ' ' . $address->getAddressLine2(),
      'city' => $address->getLocality(),
      'state' => $address->getAdministrativeArea(),
      'zip' => $address->getPostalCode(),
      'country' => $address->getCountryCode(),
      'phoneNumber' => $profile->field_phone1->value,
    ];
  }

  /**
   * Parse the order line items data.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order object.
   *
   * @return array
   *   Return the parsed order items list as an associated array (if available).
   */
  protected function parseLineItems(OrderInterface $order): array {
    $items = [];
    foreach ($order->getItems() as $orderItem) {
      /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $purchasableEntity */
      $purchasableEntity = $orderItem->getPurchasedEntity();

      $items[] = [
        'sku' => $purchasableEntity->getSku(),
        'name' => $purchasableEntity->getTitle(),
        'price' => $purchasableEntity->getPrice()->getNumber(),
        'quantity' => $orderItem->getQuantity(),
      ];
    }
    return $items;
  }

  /**
   * Parse the order credit card data.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order object.
   *
   * @return array
   *   The parsed order payment credit card information (if available).
   */
  protected function parseCreditCard(OrderInterface $order): array {
    if (!$order->payment_method->entity) {
      return [];
    }
    // Get the expire date from the credit card payment info.
    $expireDate = (new DrupalDateTime())->createFromTimestamp($order->payment_method->entity->expires->value);

    return [
      'last4' => $order->payment_method->entity->card_number->value,
      'expirationDate' => $expireDate->format('md'),
      'bin' => $order->payment_method->entity->bin_number ? $order->payment_method->entity->bin_number->value : '',
      'cardType' => $order->payment_method->entity->card_type->value,
    ];
  }

  /**
   * Prepare the NoFraud transaction payload.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order object.
   *
   * @return array
   *   The NoFraud request payload.
   */
  protected function prepareTransactionRequestPayload(OrderInterface $order): array {
    // Calculate the shipping amount.
    $shippingAmount = $order->getTotalPrice()->subtract($order->getSubtotalPrice());
    // Get the order billing profile data.
    $billingProfile = !empty($order->collectProfiles()['billing']) ? $order->collectProfiles()['billing'] : NULL;
    // Get the order shipping profile data.
    $shippingProfile = !empty($order->collectProfiles()['shipping']) ? $order->collectProfiles()['shipping'] : NULL;

    $payload = [
      // The NoFraud API key.
      'nfToken' => $this->apiKey,
      // The total order price (including the adjustments like shipping).
      'amount' => $order->getTotalPrice()->getNumber(),
      // The shipping amount of the order.
      'shippingAmount' => $shippingAmount->getNumber(),
      'shippingMethod' => 'Standard Shipping',
      'customer' => ['email' => $order->getEmail()],
      'customerIP' => $order->getIpAddress(),
      'order' => ['invoiceNumber' => $order->getOrderNumber()],
      'avsResultCode' => $this->getAvsResponseLabel($order),
      'cvvResultCode' => $this->getCvvResponseLabel($order),
      'payment' => ['creditCard' => $this->parseCreditCard($order)],
      'lineItems' => $this->parseLineItems($order),
    ];

    if ($this->parseProfileData($billingProfile)) {
      $payload['billTo'] = $this->parseProfileData($billingProfile);
    }

    if ($this->parseProfileData($shippingProfile)) {
      $payload['shipTo'] = $this->parseProfileData($shippingProfile);
    }

    return $payload;
  }

  /**
   * {@inheritdoc}
   */
  public static function getTransactionStatuses(): array {
    return [
      'pass' => new TranslatableMarkup('Pass'),
      'fail' => new TranslatableMarkup('Fail'),
      'review' => new TranslatableMarkup('Review'),
      'fraudulent' => new TranslatableMarkup('Fraudulent'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getNoFraudTransactionByOrder(OrderInterface $order) {
    $nofraud_transaction = $this
      ->entityTypeManager
      ->getStorage('nofraud')
      ->loadByProperties(['order_id' => $order->id()]);

    if (count($nofraud_transaction)) {
      return reset($nofraud_transaction);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function cancelTransaction(OrderInterface $order) {
    try {
      /** @var \Drupal\nofraud\Entity\NoFraudTransaction $nofraud_transaction */
      $nofraud_transaction = $this->getNoFraudTransactionByOrder($order);
      if ($nofraud_transaction) {
        $response = $this->httpClient->post(
          self::CANCELLATION_URL,
          [
            RequestOptions::JSON => [
              'nf_token' => $this->apiKey,
              'transaction_id' => $nofraud_transaction->get('nofraud_id')->value,
            ],
          ]
        );

        $code = $response->getStatusCode();
        if ($code == '200') {
          // Set the NoFraud record from Drupal side as canceled once it was
          // canceled/deleted from Nofraud system.
          $nofraud_transaction->setCanceled();
          $nofraud_transaction->save();
        }
      }
    }
    catch (ClientException $e) {
      if ($this->debug) {
        $this->getLogger('nofraud')->error($e->getMessage());
      }
    }
    catch (ServerException $e) {
      if ($this->debug) {
        $this->getLogger('nofraud')->error($e->getMessage());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function sendTransaction(OrderInterface $order) {
    try {
      $response = $this->httpClient->post(
        $this->apiUrl,
        [RequestOptions::JSON => $this->prepareTransactionRequestPayload($order)]
      );

      $code = $response->getStatusCode();
      if ($code == '200') {
        $response = Json::decode((string) $response->getBody(), TRUE);
        // Record a new NoFraud transaction based on the new placed order.
        $nofraud_transaction = $this->entityTypeManager->getStorage('nofraud')->create([
          'label' => $order->label() . ' : ' . $response['id'],
          'nofraud_id' => $response['id'],
          'order_id' => $order->id(),
          'status' => $response['decision'],
        ]);
        $nofraud_transaction->save();
      }
    }
    catch (ClientException $e) {
      if ($this->debug) {
        $this->getLogger('nofraud')->error($e->getMessage());
      }
    }
    catch (ServerException $e) {
      if ($this->debug) {
        $this->getLogger('nofraud')->error($e->getMessage());
      }
    }
  }

  /**
   * Get the payment entity from the order as context.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order object.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface|NULL
   *   The payment entity or NULL.
   */
  protected function getPaymetFromOrder(OrderInterface $order) {
    /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    /** @var ?\Drupal\commerce_payment\Entity\PaymentInterface[] $payments */
    $payments = $payment_storage->loadMultipleByOrder($order);

    if (!$payments) {
      return NULL;
    }
    return reset($payments);
  }

  /**
   * Returns the AVS response code.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order object.
   *
   * @return string
   *   The AVS response label string.
   */
  protected function getAvsResponseLabel(OrderInterface $order): string {
    $payment = $this->getPaymetFromOrder($order);

    if (!$payment) {
      // If the payment is not available we return 'Address Unavailable'.
      return 'U';
    }
    $avs_response_code_label = $payment->getAvsResponseCodeLabel();

    // The avs label might need to be altered to match the expected format for
    // nofraud. This allows modules to implement an alter hook and do the
    // changes.
    $this->moduleHandler->alter('nofraud_avs_label', $avs_response_code_label);

    return $avs_response_code_label;
  }

  /**
   * Returns the AVS response code.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order object.
   *
   * @return string
   *   The CVV response label string.
   */
  protected function getCvvResponseLabel(OrderInterface $order): string {
    $payment = $this->getPaymetFromOrder($order);

    if (!$payment) {
      // If the payment is not available we return 'Not processed'.
      return 'P';
    }

    if ($payment->hasField('cvv_response_code_label')) {
      $cvv_response_code_label = $payment->get('cvv_response_code_label')->value;

      // The cvv label might need to be altered to match the expected format for
      // nofraud. This allows modules to implement an alter hook and do the
      // changes.
      $this->moduleHandler->alter('nofraud_cvv_label', $cvv_response_code_label);

      return $cvv_response_code_label;
    }
    return 'P';
  }

}
