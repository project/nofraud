<?php

namespace Drupal\nofraud;

use Drupal\nofraud\Services\NoFraudInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * The order event subscriber class for NoFraud logic.
 */
class NoFraudOrderSubscriber implements EventSubscriberInterface {

  /**
   * The NoFraud service.
   *
   * @var \Drupal\nofraud\Services\NoFraudInterface
   */
  protected NoFraudInterface $noFraud;

  /**
   * Constructs a new NoFraudOrderSubscriber object.
   *
   * @param \Drupal\nofraud\Services\NoFraudInterface $nofraud
   *   The NoFraud service.
   */
  public function __construct(NoFraudInterface $nofraud) {
    $this->noFraud = $nofraud;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_order.cancel.post_transition' => ['onCancel'],
      'commerce_order.place.post_transition' => ['onPlace'],
    ];
  }

  /**
   * The order cancel event.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onCancel(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    // Cancel the NoFraud transaction record.
    $this->noFraud->cancelTransaction($order);
  }

  /**
   * The order place event.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onPlace(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    // Send the order to the NoFraud service in order to add a new transaction.
    if (!$this->noFraud->getNoFraudTransactionByOrder($order)) {
      $this->noFraud->sendTransaction($order);
    }
  }

}
