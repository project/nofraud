<?php

namespace Drupal\nofraud\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

/**
 * Event after the decision has been received but before saving.
 */
class BeforeReviewDecisionEvent extends Event {

  /**
   * NoFraudTransaction entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected EntityInterface $entity;

  /**
   * Constructs a new class instance.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The NoFraud decision.
   */
  public function __construct(EntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Retrieves the NoFraud transaction entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   EntityInterface.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

}
