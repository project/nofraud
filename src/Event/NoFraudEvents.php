<?php

declare(strict_types=1);

namespace Drupal\nofraud\Event;

/**
 * Defines NoFraud events.
 */
final class NoFraudEvents {

  /**
   * Event before the decison is saved.
   *
   * @Event
   *
   * @see \Drupal\nofraud\Event\BeforeReviewDecisionEvent
   */
  const BEFORE_REVIEW_DECISION = BeforeReviewDecisionEvent::class;

  /**
   * Event after the descison is saved.
   *
   * @Event
   *
   * @see \Drupal\nofraud\Event\PostReviewDecisionEvent
   */
  const POST_REVIEW_DECISION = PostReviewDecisionEvent::class;

}
