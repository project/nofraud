<?php

declare(strict_types=1);

namespace Drupal\nofraud\Controller;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\nofraud\Entity\NoFraudTransaction;
use Drupal\nofraud\Event\BeforeReviewDecisionEvent;
use Drupal\nofraud\Event\NoFraudEvents;
use Drupal\nofraud\Event\PostReviewDecisionEvent;
use Drupal\nofraud\Services\NoFraud;
use Drupal\nofraud\Services\NoFraudInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for NoFraud routes.
 */
class NoFraudController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * The NoFraud service.
   *
   * @var \Drupal\nofraud\Services\NoFraudInterface
   */
  protected NoFraudInterface $noFraud;

  /**
   * The event dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected ContainerAwareEventDispatcher $eventDispatcher;

  /**
   * The currently active route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $currentRouteMatch;

  /**
   * Constructs a new NoFraudController object.
   *
   * @param \Drupal\nofraud\Services\NoFraudInterface $nofraud
   *   The NoFraud service.
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $event_dispatcher
   *   Event Dispatcher service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The currently active route match object.
   */
  public function __construct(NoFraudInterface $nofraud, ContainerAwareEventDispatcher $event_dispatcher, RouteMatchInterface $current_route_match) {
    $this->noFraud = $nofraud;
    $this->eventDispatcher = $event_dispatcher;
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('nofraud'),
      $container->get('event_dispatcher'),
      $container->get('current_route_match')
    );
  }

  /**
   * Cancel the NoFraud transaction record from both NoFraud/Drupal sides.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Return the redirect response to the list of NoFraud transactions.
   */
  public function cancelTransaction(): RedirectResponse {
    $nofraud_id = $this->currentRouteMatch->getParameter('nofraud');
    $nofraud = $this->entityTypeManager()->getStorage('nofraud')->load($nofraud_id);
    // Prepare the redirect url after the cancelation process will be finished.
    $url = Url::fromUserInput('/admin/commerce/config/nofraud/list');
    $redirect = $url->toString();

    // A basic validation for the GET param from the url and be sure that the
    // nofraud transaction entity is properly loaded.
    if (!$nofraud instanceof NoFraudTransaction) {
      $this->messenger()->addError($this->t('Could not find a such a transaction record.'));
      return new RedirectResponse($redirect);
    }
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $nofraud->order_id->entity;

    if (!$order instanceof OrderInterface) {
      $this->messenger()->addError(
        $this->t(
          'There is no possibility to cancel the transaction <em>@name</em> due to the missing order reference.',
          ['@name' => $nofraud->label()]
        )
      );
      return new RedirectResponse($redirect);
    }

    // Cancel the NoFraud transaction record.
    if ($this->noFraud->getNoFraudTransactionByOrder($order)) {
      $this->noFraud->cancelTransaction($order);
      $this->messenger()->addStatus(
        $this->t(
          'The transaction <em>@name</em> has been canceled with success.',
          ['@name' => $nofraud->label()]
        )
      );
    }
    return new RedirectResponse($redirect);
  }

  /**
   * A webhook callback used to update the NoFraud transaction status.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The response as a json format for NoFraud service.
   */
  public function webhookUpdateStatus(Request $request): JsonResponse {
    // Fetch the NoFraud API key from the selected header.
    $api_key = $request->headers->get('NO-FRAUD-API-KEY');
    if (!$api_key) {
      return new JsonResponse(['message' => $this->t('Bad request. Missing the api key.')], 400);
    }
    // Validation for the provided api key from NoFraud service.
    if ($this->config('nofraud.settings')->get('apikey') !== $api_key) {
      return new JsonResponse(['message' => $this->t('Bad request. Invalid api key provided.')], 400);
    }

    $message = json_decode($request->getContent(), FALSE);
    // Add some basic validation.
    if (empty($message)) {
      return new JsonResponse(['message' => $this->t('Bad request. Missing required data.')], 400);
    }
    // Get the decision value passed from NoFraud service.
    $decision = $message->decision;
    // Get the decision ID passed from NoFraud service.
    $decision_id = $message->decision_id;

    // Validate the provided decision ID value.
    if (!$decision_id) {
      return new JsonResponse(['message' => $this->t('Please, provide a valid decision ID (nofraud transaction ID).')], 400);
    }
    // Validate the provided decision value.
    if (!$decision || ($decision && !in_array($decision, array_keys(NoFraud::getTransactionStatuses())))) {
      return new JsonResponse(['message' => $this->t('Please, provide a valid decision value.')], 400);
    }

    // Check if there is such a NoFraud transaction in the Drupal system.
    $nofraud_transaction = $this
      ->entityTypeManager()
      ->getStorage('nofraud')
      ->loadByProperties(['nofraud_id' => $decision_id]);

    if (!empty($nofraud_transaction)) {
      $nofraud_transaction = reset($nofraud_transaction);
    }

    // Could not find such a transaction record.
    if (!$nofraud_transaction instanceof NoFraudTransaction) {
      return new JsonResponse(['message' => $this->t('Could not find such a transaction.')], 400);
    }

    // Update the NoFraud transaction record status.
    $nofraud_transaction->setStatus($decision);

    $event = new BeforeReviewDecisionEvent($nofraud_transaction);
    $this->eventDispatcher->dispatch($event, NoFraudEvents::BEFORE_REVIEW_DECISION);

    $nofraud_transaction->save();

    $event = new PostReviewDecisionEvent($nofraud_transaction);
    $this->eventDispatcher->dispatch($event, NoFraudEvents::POST_REVIEW_DECISION);

    // Return the success response.
    return new JsonResponse(
      [
        'message' => $this->t(
          'The transaction with the @id was updated to @status with success!',
          ['@id' => $decision_id, '@status' => $decision]
        ),
      ]
    );
  }

}
