<?php

namespace Drupal\nofraud;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Url;

/**
 * Provides a list controller for nofraud transaction entity.
 *
 * @ingroup nofraud
 */
class NoFraudListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function getOperations(EntityInterface $entity) {
    $operations = parent::getOperations($entity);
    $operations['view'] = [
      'title' => $this->t('View'),
      'url' => $this->ensureDestination($entity->toUrl('canonical')),
    ];
    $operations['cancel_transaction'] = [
      'title' => $this->t('Cancel Transaction'),
      'url' => $this->ensureDestination(Url::fromRoute('nofraud.cancel_transaction', ['nofraud' => $entity->id()])),
    ];
    return $operations;
  }

}
